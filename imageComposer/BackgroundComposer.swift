//
//  BackgroundComposer.swift
//  imageComposer
//
//  Created by Albert Musagitov on 16/07/2019.
//  Copyright © 2019 Albert  Musagitov. All rights reserved.
//

import UIKit

class BackgroundComposer {
    
    static let shared = BackgroundComposer()
    
    static var operationQueue = OperationQueue()
    
    private var layer: CALayer?
    
    static func addToRenderQueue(composeImageInfo: ComposeImageInfo) {
        BackgroundComposer.operationQueue.addOperation {
            BackgroundComposer.setupRenderer(withComposeImageInfo: composeImageInfo)
        }
    }
    static let imageProcessed = Notification.Name("imageProccessed")
    
    static func setupRenderer(withComposeImageInfo composeImageInfo: ComposeImageInfo) {
        guard let mainImage = UIImage(named: composeImageInfo.mainImageName) else { return }
        
        if composeImageInfo.composeObjects.isEmpty {
            NotificationCenter.default.post(name: BackgroundComposer.imageProcessed, object: nil, userInfo: ["image": mainImage])
            return
        }
        
        let renderer = BackgroundComposer()
        renderer.setup(with: composeImageInfo)
    }
    
    fileprivate func setup(with composeImageInfo: ComposeImageInfo) {
        guard self.layer == nil else {
            return
        }
        
        guard let mainImage = UIImage(named: composeImageInfo.mainImageName) else { return }
        
        let layer = CALayer()
        layer.frame = CGRect(origin: .zero, size: mainImage.size)
        layer.contents = mainImage.cgImage
        
        func createTextLayer(_ textInfo: TextToCompose) -> CALayer? {
            
            // Text layer
            let textLayer = CATextLayer()
            textLayer.string = textInfo.content
            textLayer.frame = textInfo.frame
            textLayer.alignmentMode = .natural
            textLayer.backgroundColor = UIColor.clear.cgColor
            textLayer.allowsFontSubpixelQuantization = true
            textLayer.rasterizationScale = UIScreen.main.scale
            textLayer.contentsScale = UIScreen.main.scale
            
            return textLayer
        }
        
        func createBrushLayer(_ drawingInfo: DrawingToCompose) -> CALayer? {
//            let bezierPath = UIBezierPath(catmullRomInterpolatedPoints: arrPoints, closed: false, alpha: 0.5)
            
            let shapeLayer = CAShapeLayer()
            shapeLayer.frame = drawingInfo.content.bounds
            shapeLayer.path = drawingInfo.content.cgPath
            shapeLayer.strokeColor = drawingInfo.color.cgColor
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.lineWidth = drawingInfo.content.lineWidth
            shapeLayer.lineCap = .round
            shapeLayer.lineJoin = .round
            shapeLayer.position = CGPoint.zero
            shapeLayer.isOpaque = true
            
            return shapeLayer
        }
        
        func createImageLayer(_ imageInfo: ImageToCompose) -> CALayer? {
            
            guard let image = UIImage(named: imageInfo.content) else { return nil }
            
            let imageLayer = CALayer()
            imageLayer.frame = imageInfo.frame
            
            let maskLayer = CALayer()
            maskLayer.frame = imageLayer.bounds
            maskLayer.contents = image.cgImage
            
            imageLayer.mask = maskLayer
            imageLayer.backgroundColor = imageInfo.color.cgColor
            imageLayer.isOpaque = true
            imageLayer.drawsAsynchronously = true
            
            return imageLayer
        }
        
        for objectToCompose in composeImageInfo.composeObjects {
            if let textToCompose = objectToCompose as? TextToCompose {
                guard let textLayer = createTextLayer(textToCompose) else { continue }
                layer.addSublayer(textLayer)
            } else if let drawingToCompose = objectToCompose as? DrawingToCompose {
                guard let brushLayer = createBrushLayer(drawingToCompose) else { continue }
                layer.addSublayer(brushLayer)
            } else if let imageToCompose = objectToCompose as? ImageToCompose {
                guard let symbolLayer = createImageLayer(imageToCompose) else { continue }
                layer.addSublayer(symbolLayer)
            }
        }
        self.layer = layer
        self.render()
    }
    
    fileprivate func render() {
        guard let layer = self.layer else { return }
        
        //create a cgcontext
        let width = Int(layer.frame.size.width)
        let height = Int(layer.frame.size.height)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bytesPerPixel = 4
        let bytesPerRow = bytesPerPixel * width
        let rawData = malloc(height * bytesPerRow)
        let bitsPerComponent = 8
        guard let context = CGContext(data: rawData, width: width, height: height, bitsPerComponent: bitsPerComponent, bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue | CGImageByteOrderInfo.order32Big.rawValue) else { return }
        
        if layer.contentsAreFlipped() {
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: layer.frame.size.height)
            context.concatenate(flipVertical)
        }
        
        layer.render(in: context)
        
        self.layer = nil
        
        guard let cgImage = context.makeImage() else { return }
        
        free(rawData)
        
        let uiimage = UIImage(cgImage: cgImage)
        NotificationCenter.default.post(name: BackgroundComposer.imageProcessed, object: nil, userInfo: ["image": uiimage])
    }
}
