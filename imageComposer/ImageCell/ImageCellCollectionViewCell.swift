//
//  ImageCellCollectionViewCell.swift
//  imageComposer
//
//  Created by Albert Musagitov on 25.07.2019.
//  Copyright © 2019 Albert  Musagitov. All rights reserved.
//

import UIKit

class ImageCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet fileprivate var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setImage(_ image: UIImage?) {
        self.imageView.image = image
    }

}

extension UICollectionViewCell {
    static var cellId: String {
        let mirror = Mirror(reflecting: self)
        return "\(mirror.subjectType)".replacingOccurrences(of: ".Type", with: "")
    }
}
