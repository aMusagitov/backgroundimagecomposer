//
//  ComposeImageInfo.swift
//  imageComposer
//
//  Created by Albert Musagitov on 16.07.2019.
//  Copyright © 2019 Albert  Musagitov. All rights reserved.
//
import Foundation

class ComposeImageInfo {
    var mainImageName: String
    var composeObjects: [ObjectToCompose]
    
    init(mainImageName: String, composeObjects: [ObjectToCompose]) {
        self.mainImageName = mainImageName
        self.composeObjects = composeObjects
    }
}
