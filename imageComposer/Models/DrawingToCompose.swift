//
//  DrawingToCompose.swift
//  imageComposer
//
//  Created by Albert Musagitov on 16.07.2019.
//  Copyright © 2019 Albert  Musagitov. All rights reserved.
//

import UIKit

class DrawingToCompose: ObjectToCompose {
    var content: UIBezierPath
    
    init(frame: CGRect, color: UIColor, content: UIBezierPath) {
        self.content = content
        super.init(frame: frame, color: color)
    }
}
