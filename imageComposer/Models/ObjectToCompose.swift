//
//  ObjectToCompose.swift
//  imageComposer
//
//  Created by Albert Musagitov on 16.07.2019.
//  Copyright © 2019 Albert  Musagitov. All rights reserved.
//

import UIKit

enum ObjectToComposeType {
    case drawing
    case text
    case image
}

class ObjectToCompose: NSObject {
    var frame: CGRect
    var color: UIColor
    
    init(frame: CGRect, color: UIColor) {
        self.frame = frame
        self.color = color
    }
}
