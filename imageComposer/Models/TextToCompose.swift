//
//  TextToCompose.swift
//  imageComposer
//
//  Created by Albert Musagitov on 16.07.2019.
//  Copyright © 2019 Albert  Musagitov. All rights reserved.
//

import UIKit

class TextToCompose: ObjectToCompose {
    var content: NSAttributedString
    
    init(frame: CGRect, color: UIColor, content: NSAttributedString) {
        self.content = content
        super.init(frame: frame, color: color)
    }
}

