//
//  ViewController.swift
//  imageComposer
//
//  Created by Albert Musagitov on 16/07/2019.
//  Copyright © 2019 Albert  Musagitov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet fileprivate var collectionView: UICollectionView!
    
    var imagePaths = [String]()
    let cache = NSCache<NSIndexPath, UIImage>()

    override func viewDidLoad() {
        super.viewDidLoad()
        cache.countLimit = 100
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let mainImage = UIImage(named: "background") else { return }
        let size = mainImage.size
        DispatchQueue.global(qos: .background).async {
            for _ in 0...500 {
                var composeObjects = [ObjectToCompose]()
                for _ in 0...Int.random(in: 1...10) {
                    let randomSwiftImageSize = CGFloat.random(in: size.width/50...size.width/4)
                    let maxOriginPointX = size.width - randomSwiftImageSize
                    let maxOriginPointY = size.height - randomSwiftImageSize
                    let randomOriginX = CGFloat.random(in: 0.0...maxOriginPointX)
                    let randomOriginY = CGFloat.random(in: 0.0...maxOriginPointY)
                    let frame = CGRect(x: randomOriginX, y: randomOriginY, width: randomSwiftImageSize, height: randomSwiftImageSize)
                    let object = ImageToCompose(frame: frame, color: UIColor.random, content: "swift")
                    composeObjects.append(object)
                }
                let composeInfo = ComposeImageInfo(mainImageName: "background", composeObjects: composeObjects)
                BackgroundComposer.addToRenderQueue(composeImageInfo: composeInfo)
            }
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.reloadData()
    }
    
    private func setup() {
        setupCollectionView()
        NotificationCenter.default.addObserver(self, selector: #selector(imageReceived(_:)), name: BackgroundComposer.imageProcessed, object: nil)
    }

    private func setupCollectionView() {
        collectionView.register(UINib(nibName: ImageCellCollectionViewCell.cellId, bundle: nil), forCellWithReuseIdentifier: ImageCellCollectionViewCell.cellId)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    @objc
    private func imageReceived(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let image = userInfo["image"] as? UIImage else { return }
        
        let tempDirectory = FileManager.default.temporaryDirectory
        guard let imageData = image.pngData() else { return }
        let url = tempDirectory.appendingPathComponent("\(imagePaths.count + 1).png")
        do {
            try imageData.write(to: url)
            DispatchQueue.main.async { [weak self] in
                guard let `self` = self else { return }
                self.collectionView.performBatchUpdates({
                    self.imagePaths.append(url.path)
                    self.collectionView.insertItems(at: [IndexPath(item: self.imagePaths.count - 1, section: 0)])
                }, completion: nil)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}



// MARK: - UICollectionView Methods
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagePaths.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width * 0.25
        let height = width * 0.625
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCellCollectionViewCell.cellId, for: indexPath) as? ImageCellCollectionViewCell else { return UICollectionViewCell() }
        
        if let imageFromCache = cache.object(forKey: indexPath as NSIndexPath) {
            cell.setImage(imageFromCache)
        } else {
            cell.setImage(nil)
            DispatchQueue.global().async { [weak self] in
                guard let `self` = self else { return }
                // Background Thread
                let imagePath = self.imagePaths[indexPath.item]
                guard let image = UIImage(contentsOfFile: imagePath) else { return }
                self.cache.setObject(image, forKey: indexPath as NSIndexPath)
                DispatchQueue.main.async {
                    cell.setImage(image)
                }
            }
        }
        
        return cell
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
